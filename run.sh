#!/bin/bash

arm64_image="${ARM64_IMAGE:-"registry.opensource.zalan.do/acid/spilo-cdp-arm64-14:latest"}"
amd64_image="${AMD64_IMAGE:-"registry.opensource.zalan.do/acid/spilo-14:latest"}"
target_image="${TARGET_IMAGE:-"docker.io/miketth/spilo-14:latest"}"

podman manifest create "$target_image"
podman manifest add "$target_image" "$arm64_image"
podman manifest add "$target_image" "$amd64_image"
podman push "$target_image"

